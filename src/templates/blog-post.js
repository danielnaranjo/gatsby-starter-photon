import React from "react";
import Helmet from "react-helmet";
import Link from "gatsby-link";

import pic01 from '../assets/images/pic01.jpg'

class BlogPostTemplate extends React.Component {
    render() {
        const post = this.props.data.markdownRemark;
        const siteTitle = this.props.data.site.siteMetadata.title;

        return (
            <div className="col-4">
                <span className="image fit"><img src={pic01} alt="" /></span>
                <Helmet title={`${post.frontmatter.title} | ${siteTitle}`} />
                <h3>{post.frontmatter.title}</h3>
                <p>{post.frontmatter.date}</p>
                <div dangerouslySetInnerHTML={{ __html: post.html }} />
                <ul className="actions">
                    <li><a href="#" className="button">More</a></li>
                </ul>
                <hr />
            </div>


        );
    }
}
export default BlogPostTemplate;

export const pageQuery = graphql`
    query BlogPostByPath($path: String!) {
        site {
            siteMetadata {
                title
                author
            }
        }
        markdownRemark(frontmatter: { path: { eq: $path } }) {
            id
            html
            frontmatter {
                title
                date(formatString: "MMMM DD, YYYY")
            }
        }
    }
`;
